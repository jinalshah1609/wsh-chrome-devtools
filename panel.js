import './lib//react.production.min.js';
import './lib/react-dom.production.min.js';
import './lib/react-json-tree.min.js';
import { Panel } from './app/Panel.js';
import { html } from './app/util.js';

// @ts-ignore
ReactDOM.render(html`<${Panel} />`, document.body);
