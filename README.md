# WSH Dev Tools
Chrome extension providing dev tools for WhiteSpace Health stuff.

## Network Traffic Inspector

Shows `GetData` and `GetObject` network calls in a tree view. Before network calls will be tracked, you have to:

1. Open dev tools
2. Click `WSH Inspector` tab
3. Reload the page

![](docs/panel.png)