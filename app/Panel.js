import { DownloadQueue } from './DownloadQueue.js';
import { theme } from './theme.js';
import { html, parseResponse, sendDataToContentScript } from './util.js';

const initialCache = {
  GetObject: {},
  GetData: {},
};

// @ts-ignore
const R = React; // Aliasing so we only have to @ts-ignore once

export const Panel = () => {
  const [cache, setCache] = R.useState(initialCache);
  const [requestCount, setRequestCount] = R.useState(0);
  const [downloadCount, setDownloadCount] = R.useState(0);

  const incrementDownloadCount = R.useCallback(() => {
    setDownloadCount((i) => i + 1);
  }, []);

  const [downloadQueue] = R.useState(
    () => new DownloadQueue(incrementDownloadCount),
  );

  const onReset = R.useCallback(() => {
    setRequestCount(0);
    setDownloadCount(0);
    setCache(initialCache);
  }, []);

  const onShowInConsole = R.useCallback(() => {
    sendDataToContentScript(cache);
  }, [cache]);

  const onDownloadClick = R.useCallback(() => {
    downloadQueue.download(cache);
  }, [cache]);

  R.useEffect(() => {
    /** @param {chrome.devtools.network.Request} result */
    async function onRequestFinished(result) {
      const response = await parseResponse(result);
      if (!response) {
        return;
      }

      setRequestCount((i) => i + 1);

      setCache((cache) => ({
        ...cache,
        [response.key]: {
          ...cache[response.key],
          [response.key2]: {
            ...cache[response.key][response.key2],
            [response.key3]: response.content,
          },
        },
      }));
    }

    chrome.devtools.network.onRequestFinished.addListener(onRequestFinished);
    return () => {
      chrome.devtools.network.onRequestFinished.removeListener(
        onRequestFinished,
      );
    };
  }, []);

  return html`<div>
    <span>Requests: ${requestCount}, Downloaded: ${downloadCount}</span>
    <${
      // @ts-ignore
      ReactJsonTree
    }
      data=${cache}
      theme=${theme}
      invertTheme=${true}
      sortObjectKeys
    />
    <button onClick=${onReset}>Reset</button>
    <button onClick=${onShowInConsole}>Show in Console</button>
    <button onClick=${onDownloadClick}>Download</button>
  </div>`;
};
